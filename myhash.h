#ifndef MYHASH_H
#define MYHASH_H

#include <openssl/sha.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <string.h>

std::string myhash(char *data_signed);
void to_hex(unsigned char *hash, std::string *hash_hex);

#endif
