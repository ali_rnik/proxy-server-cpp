#include "proxyserver.h"

/*
  Constructor to set corresponding value for private variables.

  @param(int sock) : open socket which server and client connecting with each
    other.

  noReturn
*/
Request::Request(int sock)
{
  connected_sock = sock;
  method.clear(), url.clear(), version.clear();
  request_header.clear(), response_header.clear();
  file_path.clear();
  filestream.close();
}

Request::~Request()
{
  close(connected_sock);
  filestream.close();
}

/*
  This function recieve request headers and saves them in a Map. This function
   has undefined behavior on strange http request format.

  @noParamIn

  return(success) : 0;
  return(failure) : -1;
*/
int Request::recieve_request_headers()
{
  char tmp_buf[READ_MAX]; // temporary buffer passed to read() to fill
  int ret; // used to collect return value of functions
  bool is_reqline = true; // this flag checks whether passed from request line
  std::string buf, line, cur_header, cur_value, value_buf;
  std::stringstream ss; // string stream which used to trim $line
  size_t ind_beg, ind_end; // used to trim $line

  buf.clear();
  ind_beg = ind_end = 0;

  while (1) {
    memset(tmp_buf, 0, sizeof(tmp_buf));
    ret = read(connected_sock, tmp_buf, READ_MAX);
    if (ret == -1) {
      ferror(__LINE__, __func__);
      return -1;
    }
    if (ret == 0)
      break;

    buf += tmp_buf;

    if (buf.find("\r\n\r\n") != std::string::npos)
      break;
  }

  while (1) {
    ss.clear();
    cur_value.clear();

    ind_end = buf.find("\r\n", ind_beg);
    if (ind_end == std::string::npos) {
      ferror(__LINE__, __func__);
      return -1;
    }

    line = buf.substr(ind_beg, ind_end-ind_beg);
    ind_beg = ind_end+2; // size of \r\n is 2, so adding 2 cause pass from them.

    // check whether reached end of buffer
    if (line == "")
      break;

    if (is_reqline == true) {
      is_reqline = false;
      ss << line, ss >> method, ss >> url, ss >> version;
    }
    else {
      ss << line, ss >> cur_header;
      cur_header.pop_back();

      while (ss >> value_buf)
        cur_value += value_buf, cur_value += ' ';
      cur_value.pop_back();

      request_header[cur_header] = cur_value;
    }
  }

  return 0;
}

/*
  Check whether protocol is https or http, it decide based on headers set

  @noParam

  noReturn
*/
void Request::http_https_decider()
{
  if (request_header["X-Is-Https"] == "true")
    url.insert(4, "s");
}

/*
  Check in the database for existence of specified url and ip-address.
  @noParam

  return(success) : true , means found the in the database
  return(failure) : false, means not found in database or error occured
*/
bool Request::is_in_database()
{
  filestream.open(file_path);
  if (filestream.good() == 1) {
    filestream.close();
    return true;
  }
  filestream.close();
  return false;
}

/*
 TODO : write explanation.
*/
const char * Request::echo_url()
{
  return url.data();
}

/*
TODO : write explanation.
*/
void Request::set_file_path()
{
  file_path = (std::string) DATABASE_ADDRESS + "/" +
              request_header["X-Ip-Address"] + "/" +
              myhash((char *) url.data());
}

/*
TODO : write explanation.
*/
void Request::get_response_header()
{
  Xattribute *file_attr;
  std::string header_value, header_type;
  std::vector<string> vec_header;

  vec_header.clear();
  file_attr = new Xattribute(file_path);

  file_attr->listx(&vec_header);

  for (int i = 0; i < (int) vec_header.size(); i++) {
    header_value.clear();
    file_attr->getx(vec_header[i], &header_value);
    header_type = vec_header[i].substr(5);
    response_header[header_type] = header_value;
  }
}
/*
 TODO : write explanation.
*/
int Request::internet_response()
{
  curl_global_init(CURL_GLOBAL_DEFAULT);
  CURL *curl = curl_easy_init();
  CURLcode res;
  char errbuf[CURL_ERROR_SIZE] = {0};
  errbuf[0] = 0; // this line make sure which first nit of errnif
  curl_slist *chunk = NULL;
  std::string tmp_reqh; // temporary request header
  std::map<std::string, std::string>::iterator it;
  int ret;
  Userdata ud;

  ud.connected_sock = connected_sock;

  curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errbuf);
  curl_easy_setopt(curl, CURLOPT_URL, url.data());
  curl_easy_setopt(curl, CURLOPT_HEADER, 1);
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curl_write);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ud);
  curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
  curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

  for (it = request_header.begin(); it != request_header.end(); it++) {
    tmp_reqh = it->first + ": " + it->second;
    chunk = curl_slist_append(chunk, tmp_reqh.c_str());
  }
  res = curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);

  res = curl_easy_perform(curl);

  if (res != CURLE_OK) {
    ret = strlen(errbuf);
    fprintf(stderr, "\nlibcurl: (%d) ", res);
    if (ret)
      fprintf(stderr, "%s%s", errbuf,((errbuf[ret - 1] != '\n') ? "\n" : ""));
    else
      fprintf(stderr, "%s\n", curl_easy_strerror(res));
    ferror(__LINE__, __func__);
    curl_easy_cleanup(curl);
    curl_global_cleanup();
    return -1;
  }

  curl_easy_cleanup(curl);
  curl_global_cleanup();

  if (ud.is_chunk == true) {
    if (write(connected_sock, CHUNK_END, strlen(CHUNK_END)) == -1) {
      ferror(__LINE__, __func__);
      return -1;
    }
  }

  return 0;
}

/*
  This function open a file-decriptor or file-pointer which is saved in database
   and send back data to client.
*/
int Request::database_response()
{
  std::map<std::string, std::string>::iterator it;
  std::string header_data;
  long long len, ret;
  char *buf;

  header_data.clear();

  header_data += response_header["Status-Line"] + (std::string) CRLF;
  for (it = response_header.begin(); it != response_header.end(); it++) {
    if (it->first != "Status-Line")
      header_data += it->first + ": " + it->second + (std::string) CRLF;
  }
  header_data += (std::string) CRLF;
  write(connected_sock, header_data.data(), header_data.size());

  filestream.open(file_path);
  filestream.seekg(0, filestream.end);
  len = filestream.tellg();
  filestream.seekg(0, filestream.beg);

  buf = (char *) malloc(sizeof(buf)*len);

  filestream.read(buf, len);

  ret = write(connected_sock, buf, len);
  if(ret != len) {
    fprintf(stderr, "%d %s: %s", __LINE__, __func__, strerror(errno));
    return -1;
  }

  return 0;
}


