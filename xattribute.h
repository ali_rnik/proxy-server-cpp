#ifndef XATTRIBUTE_H
#define XATTRIBUTE_H

#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <sstream>

#include <stdio.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <errno.h>
#include <error.h>
#include <string.h>
#include <stdlib.h>

using namespace std;

class Xattribute
{
private:

  string m_file_path;

public:

  Xattribute(string file_path);

  int setx(string name, string value);
  int getx(string name, string* value);
  int removex(string name);
  int listx(vector<string>* attr_name);
};

#endif // XATTRIBUTE_H
