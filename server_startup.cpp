#include "proxyserver.h"

/*
  this function init server with the port passed in, it return file descriptor
   in which the server inited on it, and return -1 on failure.

  @param(int port) : port which server will bind on it.

  return(success) : file descriptor which the server inited on it.
  return(failure) : -1, which shows the function could not init server.
*/
int server_startup(int port)
{
  int a = 1; // passing this as a parameter to setsockopt
  int sockfd;
  sockaddr_in addr;

  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = inet_addr("127.0.0.1");

  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    ferror(__LINE__, __func__);
    close(sockfd);
    return -1;
  }

  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &a, sizeof(int)) == -1) {
    ferror(__LINE__, __func__);
    close(sockfd);
    return -1;
  }

  if (bind(sockfd, (sockaddr*) &addr, sizeof(addr)) == -1) {
    ferror(__LINE__, __func__);
    close(sockfd);
    return -1;
  }

  if (listen(sockfd, 10) == -1) {
    ferror(__LINE__, __func__);
    close(sockfd);
    return -1;
  }

  return sockfd;
}
