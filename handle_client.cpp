#include "proxyserver.h"

/*
  This function handling client, with opened socket passed to it. after function
   finished , we must assure that client opened socket will be closed by the
   server.

  @param(void* arg) : opened socket which immedietly free after passing it to
    another function.

  noReturn
*/
void* handle_client(void* arg)
{
  int connected_sock;
  Request* req;

  connected_sock = *(int*) arg;
  free(arg);

  req = new Request(connected_sock);

  req->recieve_request_headers();
  req->http_https_decider();
  req->set_file_path();

  printf("Client with the following URL accepted : %s\n\n", req->echo_url());
  if (req->is_in_database()) {
    req->get_response_header();
    req->database_response();
    printf("Client with following URL Done via Database: %s\n\n", req->echo_url());
  }
  else {
    //req->internet_response();
    printf("Not handled!");
  }



  delete req;
}
